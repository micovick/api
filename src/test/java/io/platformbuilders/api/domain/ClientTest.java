package io.platformbuilders.api.domain;

import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ClientTest {
    @Test
    void givenClient_whenCreated_thenRequiredFieldsPopulated() {
        Client client = new Client("Jhon Doe", "88888888888", LocalDate.of(1991,01,01));
        assertEquals("Jhon Doe", client.getName());
        assertEquals("88888888888", client.getCpf());
        assertEquals(LocalDate.of(1991,01,01), client.getBirthDate());
    }

    @Test
    void givenClientWithWrongParameters_whenCreated_thenException(){
        assertThrows(ConstraintViolationException.class, () -> new Client("Jhon Doe", "1", LocalDate.of(1991,01,01)));
    }
}
