package io.platformbuilders.api.application.service.client;

import io.platformbuilders.api.application.port.in.client.UpdateClientUseCase;
import io.platformbuilders.api.application.port.in.client.UpdatePartialClientUseCase;
import io.platformbuilders.api.application.port.out.client.UpdateClientPort;
import io.platformbuilders.api.application.port.out.client.UpdatePartialClientPort;
import io.platformbuilders.api.domain.Client;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.BDDMockito.then;

public class UpdateClientServiceTest {
    private final UpdateClientPort updateClientPort = Mockito.mock(UpdateClientPort.class);
    private final UpdateClientUseCase updateClientService = new UpdateClientService(updateClientPort);

    @Test
    void givenProperData_thenUpdateRequestSuccess() {
        Long id = 1L;
        Client client = new Client("Jane Doe", "99999999999", LocalDate.of(1990,11,1));

        Optional<Client> result = updateClientService.update(new UpdateClientUseCase.UpdateClientCommand(id, client));

        then(updateClientPort).should().update(id, client);
    }
}
