package io.platformbuilders.api.application.service.client;

import io.platformbuilders.api.application.port.in.client.ListClientUseCase;
import io.platformbuilders.api.application.port.out.client.ListClientPort;
import io.platformbuilders.api.domain.Client;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.*;

import java.util.List;

import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.when;

public class ListClientServiceTest {
    private final ListClientPort listClientPort = Mockito.mock(ListClientPort.class);
    private final ListClientUseCase listClientService = new ListClientService(listClientPort);

    @Test
    void whenRequestDone_thenListRequested(){
        Integer page = 0;
        Integer size = 20;
        String orderBy = "name";
        String order = "asc";

        PageImpl clientPage = new PageImpl<>(List.of(), PageRequest.of(page, size, Sort.Direction.fromString(order), orderBy), 0);

        when(listClientPort.findAllWithFilters("Jane", null, PageRequest.of(page, size, Sort.Direction.fromString(order), orderBy))).thenReturn(clientPage);

        listClientService.findAllWithFilters("Jane", null, PageRequest.of(page, size, Sort.Direction.fromString(order), orderBy));

        then(listClientPort).should().findAllWithFilters("Jane", null, PageRequest.of(page, size, Sort.Direction.fromString(order), orderBy));
    }
}
