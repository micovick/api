package io.platformbuilders.api.application.service.client;

import io.platformbuilders.api.application.port.in.client.CreateClientUseCase;
import io.platformbuilders.api.application.port.out.client.CreateClientPort;
import io.platformbuilders.api.domain.Client;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.LocalDate;
import java.util.Optional;

import static org.mockito.BDDMockito.then;

public class CreateClientServiceTest {

    private final CreateClientPort createClientPort = Mockito.mock(CreateClientPort.class);
    private final CreateClientUseCase createClientService = new CreateClientService(createClientPort);

    @Test
    void givenClient_whenCommandIssued_thenCreateRequestDone(){
        Client client = new Client("Jhon Doe", "88888888888", LocalDate.of(1991,01,01));

        Optional<Client> result = createClientService.create(new CreateClientUseCase.CreateClientCommand(client));

        then(createClientPort).should().create(client);
    }
}
