package io.platformbuilders.api.application.service.client;

import io.platformbuilders.api.application.port.in.client.UpdatePartialClientUseCase;
import io.platformbuilders.api.application.port.out.client.UpdatePartialClientPort;
import io.platformbuilders.api.domain.Client;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

import static org.mockito.BDDMockito.then;

public class UpdatePartialClientServiceTest {
    private final UpdatePartialClientPort updatePartialClientPort = Mockito.mock(UpdatePartialClientPort.class);
    private final UpdatePartialClientUseCase updatePartialClientService = new UpdatePartialClientService(updatePartialClientPort);

    @Test
    void givenProperData_thenUpdateRequestSuccess() {
        Long id = 1L;
        Map<String, Object> updates = new LinkedHashMap<>();
        updates.put("name", "Pillows");

        Optional<Client> result = updatePartialClientService.updatePartial(new UpdatePartialClientUseCase.UpdatePartialCommand(id, updates));

        then(updatePartialClientPort).should().updatePartial(id, updates);
    }
}
