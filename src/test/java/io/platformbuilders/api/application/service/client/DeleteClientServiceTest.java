package io.platformbuilders.api.application.service.client;

import io.platformbuilders.api.application.port.in.client.DeleteClientUseCase;
import io.platformbuilders.api.application.port.out.client.DeleteClientPort;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

public class DeleteClientServiceTest {

    private final DeleteClientPort deleteClientPort = Mockito.mock(DeleteClientPort.class);
    private final DeleteClientUseCase deleteClientService = new DeleteClientService(deleteClientPort);

    @Test
    void givenProperData_thenDeleteRequestSuccess() {
        doReturn(true).when(deleteClientPort).delete(1l);

        boolean result = deleteClientService.delete(new DeleteClientUseCase.DeleteClientCommand(1l));

        assertEquals(true, result);
    }

}
