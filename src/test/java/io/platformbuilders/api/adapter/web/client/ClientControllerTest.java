package io.platformbuilders.api.adapter.web.client;

import io.platformbuilders.api.adapter.web.BaseIT;
import org.junit.jupiter.api.Test;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

@Transactional
public class ClientControllerTest extends BaseIT {

    @Test
    void givenClient_whenPost_thenCreated() {
        Map<String, Object> request = new HashMap<>();
        request.put("name", "Feathers");
        request.put("cpf", "12345678955");
        request.put("birth_date", "1990-01-01");

        given().contentType("application/json")
                .body(request)
                .when().log().all()
                .post(getUri() + "/client")
                .then()
                .statusCode(201);
    }

    @Test
    void givenInvalidBirthDate_whenPost_thenBadRequest() {
        Map<String, Object> request = new HashMap<>();
        request.put("name", "Feathers");
        request.put("cpf", "12345678955");
        request.put("birth_date", "1990-101-01");

        given().contentType("application/json")
                .body(request)
                .when().log().all()
                .post(getUri() + "/client")
                .then()
                .statusCode(400);
    }

    @Test
    void givenClient_whenPatch_thenNoContent() {
        Map<String, Object> request = new HashMap<>();
        request.put("name", "Feathers");
        request.put("cpf", "12345678955");
        request.put("birth_date", "1990-01-01");

        Integer id = given().contentType("application/json")
                .body(request)
                .when().log().all()
                .post(getUri() + "/client")
                .then()
                .statusCode(201).extract().jsonPath().get("id");

        request = new HashMap<>();
        request.put("name", "Jane");

        given().contentType("application/json")
                .body(request)
                .when()
                .patch(getUri() + "/client/" + id)
                .then()
                .statusCode(204);
    }

    @Test
    void givenWrongClientId_whenPatch_thenNotFound(){
        Map<String, Object> request = new HashMap<>();
        request.put("name", "Jane");
        given().contentType("application/json")
                .body(request)
                .when()
                .patch(getUri() + "/client/" + 9999)
                .then()
                .statusCode(404);

    }

    @Test
    void givenClient_whenPut_thenNoContent() {
        Map<String, Object> request = new HashMap<>();
        request.put("name", "Feathers");
        request.put("cpf", "12345678955");
        request.put("birth_date", "1990-01-01");

        Integer id = given().contentType("application/json")
                .body(request)
                .when().log().all()
                .post(getUri() + "/client")
                .then()
                .statusCode(201).extract().jsonPath().get("id");

        ClientJson clientJson = new ClientJson();
        clientJson.setName("John");
        clientJson.setCpf("99999999999");
        clientJson.setBirthDate("1990-01-01");

        given().contentType("application/json")
                .body(clientJson)
                .when()
                .put(getUri() + "/client/" + id)
                .then()
                .statusCode(204);
    }

    @Test
    void givenWrongClientId_whenUpdate_thenNotFound(){
        ClientJson clientJson = new ClientJson();
        clientJson.setName("John");
        clientJson.setCpf("99999999999");
        clientJson.setBirthDate("1990-01-01");
        given().contentType("application/json")
                .body(clientJson)
                .when()
                .put(getUri() + "/client/" + 9999)
                .then()
                .statusCode(404);
    }

    @Test
    void givenClient_whenClientDeleted_thenOk() {
        Map<String, Object> request = new HashMap<>();
        request.put("name", "Feathers");
        request.put("cpf", "12345678955");
        request.put("birth_date", "1990-01-01");

        ClientResponse clientResponse = given().contentType("application/json")
                .body(request)
                .when().log().all()
                .post(getUri() + "/client")
                .then()
                .statusCode(201).extract().jsonPath().getObject("", ClientResponse.class);

        given().contentType("application/json")
                .when()
                .delete(getUri() +
                        "/client/" + clientResponse.getId())
                .then()
                .statusCode(204);
    }

    @Test
    void givenWrongClientId_whenClientDeleted_thenNotFound() {
        given().contentType("application/json")
                .when()
                .delete(getUri() +
                        "/client/" + 999999999)
                .then()
                .statusCode(404);
    }

    @Test
    void whenFindAll_thenListReturned() {
        given().contentType("application/json")
                .when().log().all()
                .get(getUri() +
                        "/client?page=0&size=10&name=Jane&cpf=888888&order=asc")
                .then()
                .statusCode(206);
    }
}
