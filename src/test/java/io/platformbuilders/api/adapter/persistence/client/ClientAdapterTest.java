package io.platformbuilders.api.adapter.persistence.client;

import io.platformbuilders.api.domain.Client;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@Import({ClientAdapter.class, ClientPersistenceMapper.class})
public class ClientAdapterTest {
    @Autowired
    private ClientAdapter clientAdapter;

    @Test
    void givenClient_whenCreate_thenClientReturned(){
        Client client = new Client("Jhon Doe", "88888888888", LocalDate.of(1991,01,01));

        Optional<Client> result = clientAdapter.create(client);

        assertTrue(result.isPresent());
        assertEquals("Jhon Doe", result.get().getName());
        assertEquals("88888888888", result.get().getCpf());
        assertEquals(LocalDate.of(1991,01,01), result.get().getBirthDate());
    }

    @Test
    @Sql(statements = {
            "insert into pb_client (id, deleted, name, cpf, birth_date) values (1, false, 'Jhon Doe',  '88888888888', '1991-01-01')"
    })
    void givenClient_whenDeleted_thenTrue() {
        boolean result = clientAdapter.delete(1L);

        assertTrue(result);
    }

    @Test
    void givenWrongClient_whenDeleted_thenFalse() {
        boolean result = clientAdapter.delete(9999L);

        assertFalse(result);
    }

    @Test
    @Sql(statements = {
            "insert into pb_client (id, deleted, name, cpf, birth_date) values (1, false, 'Jhon Doe',  '88888888888', '1991-01-01')"
    })
    void givenInsertedClientAndUpdates_whenUpdatePartial_thenUpdatedClientReturnedUpdated(){
        Map<String, Object> updates = new HashMap<>();
        updates.put("name", "Jane");
        updates.put("cpf", "12345678662");
        updates.put("birthDate", LocalDate.of(1997, 05, 28));
        Optional<Client> result = clientAdapter.updatePartial(1L, updates);

        assertTrue(result.isPresent());
        assertEquals("Jane", result.get().getName());
        assertEquals("12345678662", result.get().getCpf());
        assertEquals(LocalDate.of(1997,5,28), result.get().getBirthDate());
    }

    @Test
    @Sql(statements = {
            "insert into pb_client (id, deleted, name, cpf, birth_date) values (1, false, 'Jhon Doe',  '88888888888', '1991-01-01')"
    })
    void givenInsertedClient_whenUpdatePartial_thenUpdatedClientReturnedUpdated(){
        Map<String, Object> updates = new HashMap<>();

        Optional<Client> result = clientAdapter.updatePartial(1L, updates);

        assertTrue(result.isPresent());
        assertEquals("Jhon Doe", result.get().getName());
        assertEquals("88888888888", result.get().getCpf());
        assertEquals(LocalDate.of(1991,1,1), result.get().getBirthDate());
    }

    @Test
    @Sql(statements = {
            "insert into pb_client (id, deleted, name, cpf, birth_date) values (99, false, 'Jhon Doe',  '88888888888', '1991-01-01')"
    })
    void givenInsertedClientAndUpdates_whenUpdate_thenUpdatedClientReturnedUpdated(){
        Client client = new Client("Jane", "99999999999", LocalDate.of(1991,2,2));
        client.setId(99L);
        Optional<Client> result = clientAdapter.update(99L, client);

        assertTrue(result.isPresent());
        assertEquals("Jane", result.get().getName());
        assertEquals("99999999999", result.get().getCpf());
        assertEquals(LocalDate.of(1991,2,2), result.get().getBirthDate());
    }

    @Test
    @Sql(statements = {
            "insert into pb_client (id, deleted, name, cpf, birth_date) values (100, false, 'John Cenna',  '77777777777', '1991-01-01')"
    })
    void givenInsertedClient_whenFindAllWithFilters_thenUpdatedClientReturnedUpdated(){
        int page = 0;
        int size = 20;
        String orderBy = "name";
        String order = "asc";

        Page<Client> result = clientAdapter.findAllWithFilters("John", null, PageRequest.of(page, size, Sort.Direction.fromString(order), orderBy));
        assertEquals(1, result.getTotalElements());

    }
}
