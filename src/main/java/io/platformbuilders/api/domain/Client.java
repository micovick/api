package io.platformbuilders.api.domain;

import io.platformbuilders.api.utility.SelfValidating;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

@Data
@EqualsAndHashCode(callSuper = false)
public class Client extends SelfValidating<Client> {

    private Long id;

    @NotBlank
    private final String name;

    @NotNull
    @Pattern(regexp = "^[0-9]{11}")
    private final String cpf;

    @NotNull
    private final LocalDate birthDate;

    public Client(String name, String cpf, LocalDate birthDate) {
        this.name = name;
        this.cpf = cpf;
        this.birthDate = birthDate;

        validateSelf();
    }
}
