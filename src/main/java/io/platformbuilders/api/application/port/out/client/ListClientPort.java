package io.platformbuilders.api.application.port.out.client;

import io.platformbuilders.api.domain.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ListClientPort {
    Page<Client> findAllWithFilters(String name, String cpf, Pageable pageable);
}
