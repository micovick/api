package io.platformbuilders.api.application.port.in.client;

import io.platformbuilders.api.domain.Client;
import io.platformbuilders.api.utility.SelfValidating;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Map;
import java.util.Optional;

public interface UpdatePartialClientUseCase {
    Optional<Client> updatePartial(UpdatePartialCommand updatePartialCommand);

    @Value
    @EqualsAndHashCode(callSuper = false)
    class UpdatePartialCommand extends SelfValidating<UpdatePartialCommand> {

        @NotNull
        @Positive
        Long clientId;

        @NotNull
        @NotEmpty Map<String, Object> updates;

        public UpdatePartialCommand(Long clientId, Map<String, Object> updates) {
            this.clientId = clientId;
            this.updates = updates;

            validateSelf();
        }
    }
}
