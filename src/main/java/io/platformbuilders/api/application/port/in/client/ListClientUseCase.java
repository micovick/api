package io.platformbuilders.api.application.port.in.client;

import io.platformbuilders.api.adapter.web.BaseResponse;
import io.platformbuilders.api.domain.Client;

import org.springframework.data.domain.Pageable;

public interface ListClientUseCase {
    BaseResponse<Client> findAllWithFilters(String name, String cpf,  Pageable pageable);
}
