package io.platformbuilders.api.application.port.out.client;

import io.platformbuilders.api.domain.Client;

import java.util.Optional;

public interface UpdateClientPort {
    Optional<Client> update(Long id, Client client);
}
