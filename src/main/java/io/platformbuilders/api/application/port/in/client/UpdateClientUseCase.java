package io.platformbuilders.api.application.port.in.client;

import io.platformbuilders.api.domain.Client;
import io.platformbuilders.api.utility.SelfValidating;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Optional;

public interface UpdateClientUseCase {
    Optional<Client> update(UpdateClientCommand updateClientCommand);

    @Value
    @EqualsAndHashCode(callSuper = false)
    class UpdateClientCommand extends SelfValidating<UpdateClientCommand> {
        @NotNull
        @Positive
        Long clientId;

        @NotNull
        Client client;

        public UpdateClientCommand(Long clientId, Client client) {
            this.clientId = clientId;
            this.client = client;

            validateSelf();
        }
    }
}
