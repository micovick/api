package io.platformbuilders.api.application.port.in.client;

import io.platformbuilders.api.domain.Client;
import io.platformbuilders.api.utility.SelfValidating;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface CreateClientUseCase {
    Optional<Client> create(CreateClientCommand createClientCommand);

    @Value
    @EqualsAndHashCode(callSuper = false)
    class CreateClientCommand extends SelfValidating<CreateClientCommand> {
        @NotNull
        private final Client client;

        public CreateClientCommand(Client client) {
            this.client = client;
            validateSelf();
        }
    }
}
