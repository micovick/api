package io.platformbuilders.api.application.port.in.client;

import io.platformbuilders.api.utility.SelfValidating;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public interface DeleteClientUseCase {
    boolean delete(DeleteClientCommand deleteClientCommand);

    @Value
    @EqualsAndHashCode(callSuper = false)
    class DeleteClientCommand extends SelfValidating<DeleteClientCommand> {
        @NotNull
        @Positive
        Long clientId;

        public DeleteClientCommand(Long clientId) {
            this.clientId = clientId;
            validateSelf();
        }
    }
}
