package io.platformbuilders.api.application.port.out.client;

public interface DeleteClientPort {
    boolean delete(Long clientId);
}
