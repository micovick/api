package io.platformbuilders.api.application.port.out.client;

import io.platformbuilders.api.domain.Client;

import java.util.Map;
import java.util.Optional;

public interface UpdatePartialClientPort {
    Optional<Client> updatePartial(Long id, Map<String, Object> updates);
}
