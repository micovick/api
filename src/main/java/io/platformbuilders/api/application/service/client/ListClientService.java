package io.platformbuilders.api.application.service.client;

import io.platformbuilders.api.adapter.web.BaseResponse;
import io.platformbuilders.api.adapter.web.PaginationResponse;
import io.platformbuilders.api.application.port.in.client.ListClientUseCase;
import io.platformbuilders.api.application.port.out.client.ListClientPort;
import io.platformbuilders.api.domain.Client;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ListClientService implements ListClientUseCase {

    private final ListClientPort listClientPort;

    @Override
    public BaseResponse<Client> findAllWithFilters(String name, String cpf, Pageable pageable) {
        Page<Client> result = listClientPort.findAllWithFilters(name, cpf, pageable);

        PaginationResponse paginationResponse = new PaginationResponse();
        paginationResponse.setPage(result.getPageable().getPageNumber());
        paginationResponse.setSize(result.getPageable().getPageSize());
        paginationResponse.setTotalElements(result.getTotalElements());
        paginationResponse.setTotalPages(result.getTotalPages());

        return new BaseResponse<>(result.get().collect(Collectors.toList()), paginationResponse);
    }
}
