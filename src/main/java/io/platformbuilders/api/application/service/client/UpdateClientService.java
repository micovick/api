package io.platformbuilders.api.application.service.client;

import io.platformbuilders.api.application.port.in.client.UpdateClientUseCase;
import io.platformbuilders.api.application.port.out.client.UpdateClientPort;
import io.platformbuilders.api.domain.Client;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UpdateClientService implements UpdateClientUseCase {

    private final UpdateClientPort updateClientPort;

    @Override
    public Optional<Client> update(UpdateClientCommand updateClientCommand) {
        return updateClientPort.update(updateClientCommand.getClientId(), updateClientCommand.getClient());
    }
}
