package io.platformbuilders.api.application.service.client;

import io.platformbuilders.api.application.port.in.client.CreateClientUseCase;
import io.platformbuilders.api.application.port.out.client.CreateClientPort;
import io.platformbuilders.api.domain.Client;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CreateClientService implements CreateClientUseCase {

    private final CreateClientPort createClientPort;

    @Override
    public Optional<Client> create(CreateClientCommand createClientCommand) {
        return createClientPort.create(createClientCommand.getClient());
    }
}
