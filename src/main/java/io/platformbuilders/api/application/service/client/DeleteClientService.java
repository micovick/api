package io.platformbuilders.api.application.service.client;

import io.platformbuilders.api.application.port.in.client.DeleteClientUseCase;
import io.platformbuilders.api.application.port.out.client.DeleteClientPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DeleteClientService implements DeleteClientUseCase {
    private final DeleteClientPort deleteClientPort;

    @Override
    public boolean delete(DeleteClientCommand deleteClientCommand) {
        return deleteClientPort.delete(deleteClientCommand.getClientId());
    }
}
