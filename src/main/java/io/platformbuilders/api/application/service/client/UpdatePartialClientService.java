package io.platformbuilders.api.application.service.client;

import io.platformbuilders.api.application.port.in.client.UpdatePartialClientUseCase;
import io.platformbuilders.api.application.port.out.client.UpdatePartialClientPort;
import io.platformbuilders.api.domain.Client;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UpdatePartialClientService implements UpdatePartialClientUseCase {

    private final UpdatePartialClientPort updatePartialClientPort;

    @Override
    public Optional<Client> updatePartial(UpdatePartialCommand updatePartialCommand) {
        return updatePartialClientPort.updatePartial(updatePartialCommand.getClientId(), updatePartialCommand.getUpdates());
    }
}
