package io.platformbuilders.api.adapter.persistence.client;

import io.platformbuilders.api.adapter.persistence.BaseJpaEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Loader;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@RequiredArgsConstructor
@Entity(name = "ClientJpaEntity")
@Table(name = "pb_client")
@SQLDelete(sql =
        "UPDATE pb_client SET deleted = true where id = ?")
@Loader(namedQuery = "findClientById")
@NamedQuery(name = "findClientById",
        query = "SELECT p FROM ClientJpaEntity p WHERE p.id = ?1 AND p.deleted = false")
@Where(clause = "deleted = false")
public class ClientJpaEntity extends BaseJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String cpf;

    private LocalDate birthDate;
}
