package io.platformbuilders.api.adapter.persistence.client;

import io.platformbuilders.api.domain.Client;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ClientPersistenceMapper {

    public ClientJpaEntity mapToJpaEntity(Client client) {
        ClientJpaEntity clientJpaEntity = new ClientJpaEntity();
        clientJpaEntity.setName(client.getName());
        clientJpaEntity.setCpf(client.getCpf());
        clientJpaEntity.setBirthDate(client.getBirthDate());
        clientJpaEntity.setId(client.getId());

        return clientJpaEntity;
    }

    public Client mapToDomainEntity(ClientJpaEntity clientJpaEntity){
        Client client = new Client(clientJpaEntity.getName(), clientJpaEntity.getCpf(), clientJpaEntity.getBirthDate());
        client.setId(clientJpaEntity.getId());
        return client;
    }

    public ClientJpaEntity mapUpdatesTopJpaEntity(ClientJpaEntity clientJpaEntity, Map<String, Object> updates) {
        if (updates.containsKey("cpf")) {
            clientJpaEntity.setCpf((String) updates.get("cpf"));
        }

        if (updates.containsKey("name")) {
            clientJpaEntity.setName((String) updates.get("name"));
        }

        if (updates.containsKey("birthDate")) {
            clientJpaEntity.setBirthDate((LocalDate) updates.get("birthDate"));
        }

        return clientJpaEntity;
    }
}
