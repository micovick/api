package io.platformbuilders.api.adapter.persistence;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class BaseJpaEntity {

    private boolean deleted;

}