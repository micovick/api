package io.platformbuilders.api.adapter.persistence.client;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClientRepository extends JpaRepository<ClientJpaEntity, Long> {
    @Query("SELECT ar FROM ClientJpaEntity ar WHERE " +
            "(UPPER(name) LIKE CONCAT('%', UPPER(:name), '%') or :name is null) and " +
            "(cpf LIKE CONCAT('%', :cpf, '%') or :cpf is null)")
    Page<ClientJpaEntity> findAllWithFilters(@Param("name") String name,
                                             @Param("cpf") String cpf,
                                             Pageable pageable);
}
