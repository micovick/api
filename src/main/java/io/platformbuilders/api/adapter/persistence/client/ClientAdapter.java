package io.platformbuilders.api.adapter.persistence.client;

import io.platformbuilders.api.application.port.out.client.*;
import io.platformbuilders.api.domain.Client;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Transactional
public class ClientAdapter implements CreateClientPort, DeleteClientPort, ListClientPort, UpdateClientPort, UpdatePartialClientPort {

    private final ClientPersistenceMapper clientPersistenceMapper;
    private final ClientRepository repository;

    @Override
    public Optional<Client> create(Client client) {
        return Optional.of(clientPersistenceMapper.mapToDomainEntity(repository.save(clientPersistenceMapper.mapToJpaEntity(client))));
    }

    @Override
    public boolean delete(Long clientId) {
        Optional<ClientJpaEntity> clientJpaEntity = repository.findById(clientId);

        if (clientJpaEntity.isPresent()) {
            repository.delete(clientJpaEntity.get());
            return true;
        }

        return false;
    }

    @Override
    public Page<Client> findAllWithFilters(String name, String cpf, Pageable pageable) {
        Page<ClientJpaEntity> result = repository.findAllWithFilters(name, cpf, pageable);
        List<Client> domainEntities = result.getContent().stream().map(clientPersistenceMapper::mapToDomainEntity).collect(Collectors.toList());

        return new PageImpl<>(domainEntities, result.getPageable(), result.getTotalElements());
    }

    @Override
    public Optional<Client> update(Long id, Client client) {
        return repository.findById(id).map(clientJpaEntity -> clientPersistenceMapper.mapToDomainEntity(repository.save(clientPersistenceMapper.mapToJpaEntity(client))));
    }

    @Override
    public Optional<Client> updatePartial(Long id, Map<String, Object> updates) {
        return repository.findById(id).map(clientJpaEntity -> clientPersistenceMapper.mapToDomainEntity(repository.save(clientPersistenceMapper.mapUpdatesTopJpaEntity(clientJpaEntity, updates))));
    }
}
