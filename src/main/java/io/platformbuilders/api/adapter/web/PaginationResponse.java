package io.platformbuilders.api.adapter.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class PaginationResponse {
    @JsonProperty("page")
    public Integer page;

    @JsonProperty("total_pages")
    public Integer totalPages;

    @JsonProperty("total_elements")
    public Long totalElements;

    @JsonProperty("size")
    public Integer size;
}
