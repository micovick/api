package io.platformbuilders.api.adapter.web.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.platformbuilders.api.adapter.web.BaseResponse;
import io.platformbuilders.api.application.exception.InvalidDateFormatException;
import io.platformbuilders.api.application.port.in.client.*;
import io.platformbuilders.api.domain.Client;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/client")
@RequiredArgsConstructor
@Api(value = "Conjunto de endpoints para Criação, Recuperação, Atualização e Exclusão de Clientes")
public class ClientController {

    private final CreateClientUseCase createClientUseCase;
    private final DeleteClientUseCase deleteClientUseCase;
    private final ListClientUseCase listClientUseCase;
    private final UpdateClientUseCase updateClientUseCase;
    private final UpdatePartialClientUseCase updatePartialClientUseCase;
    private final ClientWebMapper clientWebMapper;

    private final ObjectMapper objectMapper;

    @GetMapping
    public ResponseEntity getAllWithFilters(@RequestParam(value = "name", required = false) String name,
                                            @RequestParam(value = "cpf", required = false) String cpf,
                                            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                            @RequestParam(value = "size", required = false, defaultValue = "20") Integer size,
                                            @RequestParam(value = "order_by", required = false, defaultValue = "name") String orderBy,
                                            @RequestParam(value = "order", required = false, defaultValue = "asc") String order
    ) throws JsonProcessingException {

        Map<String, String> orderByMapper = Map.of(
                "cpf", "cpf",
                "name", "name"
        );

        BaseResponse<Client> result = listClientUseCase.findAllWithFilters(name, cpf,
                PageRequest.of(page, size, Sort.Direction.fromString(order), orderByMapper.getOrDefault(orderBy, orderBy)));

        return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT).body(objectMapper.writeValueAsString(clientWebMapper.mapToResponse(result.getData())));
    }

    @PostMapping
    public ResponseEntity create(@RequestBody ClientJson clientJson) {
        try {
            Client client = clientWebMapper.mapToDomainEntity(clientJson);
            return new ResponseEntity(clientWebMapper.mapToResponse(createClientUseCase
                    .create(new CreateClientUseCase.CreateClientCommand(client)).get()),
                    HttpStatus.CREATED);
        } catch (InvalidDateFormatException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }
    }

    @PatchMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity partialUpdateGeneric(
            @RequestBody Map<String, Object> updates,
            @PathVariable Long id) {
        Optional<Client> updatedEntity =
                updatePartialClientUseCase.updatePartial(new UpdatePartialClientUseCase.UpdatePartialCommand(id, updates));

        if (updatedEntity.isPresent()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(
            @RequestBody ClientJson clientJson,
            @PathVariable Long id) {
        Optional<Client> updatedEntity =
                updateClientUseCase.update(new UpdateClientUseCase.UpdateClientCommand(id, clientWebMapper.mapToDomainEntity(clientJson)));

        if (updatedEntity.isPresent()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (deleteClientUseCase.delete(new DeleteClientUseCase.DeleteClientCommand(id))) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }
}