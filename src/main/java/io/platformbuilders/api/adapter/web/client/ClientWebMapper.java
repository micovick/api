package io.platformbuilders.api.adapter.web.client;

import io.platformbuilders.api.application.exception.InvalidDateFormatException;
import io.platformbuilders.api.domain.Client;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class ClientWebMapper {

    public Client mapToDomainEntity(ClientJson clientJson){
        if (clientJson.getBirthDate().length() != 10) {
            throw new InvalidDateFormatException("Data deve ser no formato yyyy-mm-dd");
        }

        LocalDate birthDate = LocalDate.parse(clientJson.getBirthDate());
        Client client = new Client(clientJson.getName(), clientJson.getCpf(),birthDate);
        client.setId(clientJson.getId());
        return client;
    }

    public ClientResponse mapToResponse(Client client){
        ClientResponse clientResponse = new ClientResponse();

        clientResponse.setId(client.getId());
        clientResponse.setName(client.getName());
        clientResponse.setCpf(client.getCpf());
        clientResponse.setBirthDate(client.getBirthDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        clientResponse.setAge(Period.between(client.getBirthDate(), LocalDate.now()).getYears());

        return clientResponse;
    }

    public Object mapToResponse(List<Client> clientList) {
        return clientList.stream().map(this::mapToResponse);
    }
}
