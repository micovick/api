FROM openjdk:11
MAINTAINER Euclides Cardoso Júnior <euclides.c.jr@gmail.com>

EXPOSE 8080

ADD api-0.0.1-SNAPSHOT.jar spring-boot-application.jar

ENTRYPOINT ["java", "-XX:+UnlockExperimentalVMOptions", "-Djava.security.egd=file:/dev/./urandom","-jar","/spring-boot-application.jar"]