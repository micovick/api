# English Documentation

## Execution guid

Pelase fallow the next steps to run the project on docker:

* Build the project
* Run the gradle task ___bootJar___ to create the JAR file
* Run the gradle task ___docker___ to generate the docker image
* To deploy the application run the task ___dockerComposeUp___ 
    * That will create two containers: ___docker-mysql___ for the database and  ___spring-boot-jpa-docker-webapp___ that will run the application
* To finish the application run the task  __dockerComposeDown__.

## Documentation

The API documentation is available in: [localhost:8080/swagger-ui.html](localhost:8080/swagger-ui.html).

## REST utilization

An file in a JSON format is available to import into the Postman tool. To find him search for: Platform Builders API.postman_collection.json.

## Tests

To test the application run the task ___testAndOpen___.

# Portuguese Documentation

## Guia de execução do projeto

Para executar o projeto no Docker, siga os seguintes passos:

- Realize o build do projeto;
- Execute a tarefa do gradle ___bootJar___ para criar o JAR do projeto
- Execute a tarefa ___docker___ para gerar a imagem;
- Para subir a aplicação execute a tarefa ___dockerComposeUp___;
    - Isso irá criar dois contâneires: ___docker-mysql___ para o banco de dados e ___spring-boot-jpa-docker-webapp___ o qual executará a aplicação.
- Para encerrar todos os contâneires execute a tarefa __dockerComposeDown__.

## Documentação

Foi criado uma documentação da API e está disponível em: [localhost:8080/swagger-ui.html](localhost:8080/swagger-ui.html).

## Utilização via REST

Um arquivo de importação no format JSON está diponível para importação na ferramenta Postman. Para encontra-lo busque por: Platform Builders API.postman_collection.json.

## Testes

Para executar os testes da aplicação, rode a tarefa ___testAndOpen___.